namespace Rover.Core
{
    public interface IRoverMoveCommandsExecutor
    {
        RoverCommandMoveResult ExecuteRoverCommands(string roverCommands);
    }
}