﻿using System;

namespace Rover.Core
{
    public class RoverMoveCommandsExecutor : IRoverMoveCommandsExecutor
    {
        private readonly IRover rover;
        private readonly IGrid grid;

        public RoverMoveCommandsExecutor(IRover rover, IGrid grid)
        {
            this.rover = rover;
            this.grid = grid;
        }

        public RoverCommandMoveResult ExecuteRoverCommands(string roverCommands)
        {
            var updatedPosition = rover.CurrentPosition;
            var temporaryPostition = rover.CurrentPosition;
            IObstacle obstacle = null;
            bool successful = true;
            foreach (var roverCommand in roverCommands)
            {
                switch (roverCommand)
                {
                    case 'F':
                        temporaryPostition = temporaryPostition.Forwards();
                        break;
                    case 'B':
                        temporaryPostition = temporaryPostition.Backwards();
                        break;
                    case 'R':
                        temporaryPostition = temporaryPostition.Right();
                        break;
                    case 'L':
                        temporaryPostition = temporaryPostition.Left();
                        break;
                    default:
                        throw new ArgumentOutOfRangeException(nameof(roverCommands), 
                            $"Invalid command {roverCommand}");
                }

                temporaryPostition = grid.WrapPosition(temporaryPostition);
                if (!grid.IsPositionFree(temporaryPostition, out obstacle))
                {
                    successful = false;
                    break;
                }
                updatedPosition = temporaryPostition;
            }
            rover.UpdatePosition(updatedPosition);

            return new RoverCommandMoveResult(successful, updatedPosition, obstacle);
        }
    }
}