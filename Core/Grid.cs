﻿namespace Rover.Core
{
    public class Grid : IGrid
    {
        public int MaximumXCoordinate { get; }
        public int MaximumYCoordinate { get; }

        private readonly IObstacle[][] obstacles;

        public Grid(int maximumXCoordinate, int maximumYCoordinate)
        {
            MaximumXCoordinate = maximumXCoordinate;
            MaximumYCoordinate = maximumYCoordinate;

            obstacles = new IObstacle[maximumXCoordinate + 1][];
            for (var index = 0; index < maximumXCoordinate + 1; index++)
            {
                obstacles[index] = new IObstacle[maximumYCoordinate + 1];
            }
        }

        public IPosition WrapPosition(IPosition currentPosition)
        {
            var xCoordinate = currentPosition.XCoordinate;
            var yCoordinate = currentPosition.YCoordinate;

            if (xCoordinate < 0)
            {
                xCoordinate = MaximumXCoordinate + (xCoordinate + 1);
            }
            else if (xCoordinate > MaximumXCoordinate)
            {
                xCoordinate = 0 + (xCoordinate - MaximumXCoordinate - 1);
            }

            if (yCoordinate < 0)
            {
                yCoordinate = MaximumYCoordinate + (yCoordinate + 1);
            }
            else if (yCoordinate > MaximumYCoordinate)
            {
                yCoordinate = 0 + (yCoordinate - MaximumYCoordinate - 1);
            }

            return new Position(xCoordinate, yCoordinate, currentPosition.FacingDirection);
        }

        public void AddObstacle(IObstacle obstacle, int xPosition, int yPosition)
        {
            obstacles[xPosition][yPosition] = obstacle;
        }

        public bool IsPositionFree(IPosition targetPosition, out IObstacle obstacle)
        {
            obstacle = obstacles[targetPosition.XCoordinate][targetPosition.YCoordinate];
            return obstacle == null;
        }
    }
}