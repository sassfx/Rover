using System;
using Rover.Core;

namespace Rover.Core
{
    public class Position : IPosition
    {

        public int XCoordinate { get; }

        public int YCoordinate { get; }

        public CardinalDirection FacingDirection { get; }

        public Position(int xCoordinate, int yCoordindate, CardinalDirection facingDirection)
        {
            XCoordinate = xCoordinate;
            FacingDirection = facingDirection;
            YCoordinate = yCoordindate;
        }
        
        public IPosition Right()
        {
            var updatedDirection = FacingDirection;
            switch (FacingDirection)
            {
                case CardinalDirection.North:
                    updatedDirection =  CardinalDirection.East;
                    break;
                case CardinalDirection.South:
                    updatedDirection = CardinalDirection.West;
                    break;
                case CardinalDirection.East:
                    updatedDirection = CardinalDirection.South;
                    break;
                case CardinalDirection.West:
                    updatedDirection = CardinalDirection.North;
                    break;
            }

            return new Position(XCoordinate, YCoordinate, updatedDirection);
        }
    

        public IPosition Left()
        {
            var updatedDirection = FacingDirection;
            switch (FacingDirection)
            {
                case CardinalDirection.North:
                    updatedDirection = CardinalDirection.West;
                    break;
                case CardinalDirection.South:
                    updatedDirection = CardinalDirection.East;
                    break;
                case CardinalDirection.East:
                    updatedDirection = CardinalDirection.North;
                    break;
                case CardinalDirection.West:
                    updatedDirection = CardinalDirection.South;
                    break;
            }

            return new Position(XCoordinate, YCoordinate, updatedDirection);
        }

        public IPosition Forwards()
        {
            var updatedXPosition = XCoordinate;
            var updatedYPosition = YCoordinate;

            switch (FacingDirection)
            {
                case CardinalDirection.North:
                    updatedYPosition += 1;
                    break;
                case CardinalDirection.South:
                    updatedYPosition -= 1;
                    break;
                case CardinalDirection.East:
                    updatedXPosition += 1;
                    break;
                case CardinalDirection.West:
                    updatedXPosition -= 1;
                    break;
            }

            return new Position(updatedXPosition, updatedYPosition, FacingDirection);
        }

        public IPosition Backwards()
        {
            var updatedXPosition = XCoordinate;
            var updatedYPosition = YCoordinate;

            switch (FacingDirection)
            {
                case CardinalDirection.North:
                    updatedYPosition -= 1;
                    break;
                case CardinalDirection.South:
                    updatedYPosition += 1;
                    break;
                case CardinalDirection.East:
                    updatedXPosition -= 1;
                    break;
                case CardinalDirection.West:
                    updatedXPosition += 1;
                    break;
            }

            return new Position(updatedXPosition, updatedYPosition, FacingDirection);
        }
    }
}