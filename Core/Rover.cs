﻿namespace Rover.Core
{
    public class Rover : IRover
    {
        public IPosition CurrentPosition { get; private set; }

        public Rover(IPosition initialPosition)
        {
            CurrentPosition = initialPosition;
        }
        
        public void UpdatePosition(IPosition newPosition)
        {
            CurrentPosition = newPosition;
        }
    }
}