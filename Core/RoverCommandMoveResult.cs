namespace Rover.Core
{
    public class RoverCommandMoveResult
    {
        public bool Successful { get; }
        public IPosition NewPosition { get; }
        public IObstacle EncounteredObstacle { get; }

        public RoverCommandMoveResult(bool successful, IPosition newPosition, IObstacle encounteredObstacle)
        {
            Successful = successful;
            NewPosition = newPosition;
            EncounteredObstacle = encounteredObstacle;
        }
    }
}