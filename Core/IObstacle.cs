﻿namespace Rover.Core
{
    public interface IObstacle
    {
        string Name { get; }
    }
}