namespace Rover.Core
{
    public interface IPosition
    {
        int XCoordinate { get; }
        int YCoordinate { get; }
        CardinalDirection FacingDirection { get; }
        IPosition Right();
        IPosition Left();
        IPosition Forwards();
        IPosition Backwards();
    }
}