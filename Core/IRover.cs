﻿namespace Rover.Core
{
    public interface IRover
    {
        IPosition CurrentPosition { get; }
        void UpdatePosition(IPosition newPosition);
    }
}
