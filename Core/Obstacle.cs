﻿namespace Rover.Core
{
    public class Obstacle : IObstacle
    {
        public Obstacle(string name)
        {
            Name = name;
        }

        public string Name { get; }

        protected bool Equals(Obstacle other)
        {
            return string.Equals(Name, other.Name);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Obstacle) obj);
        }

        public override int GetHashCode()
        {
            return (Name != null ? Name.GetHashCode() : 0);
        }

        public static bool operator ==(Obstacle left, Obstacle right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(Obstacle left, Obstacle right)
        {
            return !Equals(left, right);
        }
    }
}