﻿namespace Rover.Core
{
    public interface IGrid
    {
        int MaximumXCoordinate { get; }
        int MaximumYCoordinate { get; }
        IPosition WrapPosition(IPosition currentPosition);
        void AddObstacle(IObstacle obstacle, int xPosition, int yPosition);
        bool IsPositionFree(IPosition targetPosition, out IObstacle obstacle);
    }
}