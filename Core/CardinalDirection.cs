﻿namespace Rover.Core
{
    public enum CardinalDirection
    {
        North,
        South,
        East,
        West
    }
}