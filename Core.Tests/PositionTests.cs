﻿using NUnit.Framework;
using Rover.Core;

namespace Core.Tests
{
    [TestFixture]
    public class PositionTests
    {
        [TestCase(0, 0, CardinalDirection.North, 0, 1)]
        [TestCase(3, 4, CardinalDirection.South, 3, 3)]
        [TestCase(2, 2, CardinalDirection.East, 3, 2)]
        [TestCase(2, 5, CardinalDirection.West, 1, 5)]
        public void Forward_ExpectedCoordinates(int xCoordinate, int yCoordinate, CardinalDirection currentDirection,
            int expectedXCoordinate, int expectedYCoordinate)
        {
            // Arrange
            var initialPosition = new Position(xCoordinate, yCoordinate, currentDirection);

            // Act
            var resultingPosition = initialPosition.Forwards();

            // Assert
            Assert.That(resultingPosition.XCoordinate, Is.EqualTo(expectedXCoordinate));
            Assert.That(resultingPosition.YCoordinate, Is.EqualTo(expectedYCoordinate));
            Assert.That(resultingPosition.FacingDirection, Is.EqualTo(currentDirection));
        }

        [TestCase(0, 1, CardinalDirection.North, 0, 0)]
        [TestCase(3, 4, CardinalDirection.South, 3, 5)]
        [TestCase(2, 2, CardinalDirection.East, 1, 2)]
        [TestCase(2, 5, CardinalDirection.West, 3, 5)]
        public void Backwards_ExpectedCoordinates(int xCoordinate, int yCoordinate, CardinalDirection currentDirection,
            int expectedXCoordinate, int expectedYCoordinate)
        {
            // Arrange
            var initialPosition = new Position(xCoordinate, yCoordinate, currentDirection);

            // Act
            var resultingPosition = initialPosition.Backwards();

            // Assert
            Assert.That(resultingPosition.XCoordinate, Is.EqualTo(expectedXCoordinate));
            Assert.That(resultingPosition.YCoordinate, Is.EqualTo(expectedYCoordinate));
            Assert.That(resultingPosition.FacingDirection, Is.EqualTo(currentDirection));
        }

        [TestCase(CardinalDirection.North)]
        [TestCase(CardinalDirection.South)]
        [TestCase(CardinalDirection.East)]
        [TestCase(CardinalDirection.West)]
        public void ForwardsBackwards_StaysSame(CardinalDirection inputDirection)
        {
            // Arrange
            var initialPosition = new Position(3, 2, inputDirection);

            // Act
            var resultingPosition = initialPosition.Forwards().Backwards();

            // Assert
            Assert.That(resultingPosition.XCoordinate, Is.EqualTo(initialPosition.XCoordinate));
            Assert.That(resultingPosition.YCoordinate, Is.EqualTo(initialPosition.YCoordinate));
            Assert.That(resultingPosition.FacingDirection, Is.EqualTo(initialPosition.FacingDirection));
        }

        [TestCase(CardinalDirection.North, CardinalDirection.East)]
        [TestCase(CardinalDirection.South, CardinalDirection.West)]
        [TestCase(CardinalDirection.East, CardinalDirection.South)]
        [TestCase(CardinalDirection.West, CardinalDirection.North)]
        public void Right_ExpectedDirection(CardinalDirection inputDirection, CardinalDirection expectedResult)
        {
            // Arrange
            var position = new Position(3, 4, inputDirection);

            // Act
            var result = position.Right();

            // Assert
            Assert.That(result.XCoordinate, Is.EqualTo(position.XCoordinate));
            Assert.That(result.YCoordinate, Is.EqualTo(position.YCoordinate));
            Assert.That(result.FacingDirection, Is.EqualTo(expectedResult));
        }

        [TestCase(CardinalDirection.North, CardinalDirection.West)]
        [TestCase(CardinalDirection.South, CardinalDirection.East)]
        [TestCase(CardinalDirection.East, CardinalDirection.North)]
        [TestCase(CardinalDirection.West, CardinalDirection.South)]
        public void Left_ExpectedDirection(CardinalDirection inputDirection, CardinalDirection expectedResult)
        {
            // Arrange
            var position = new Position(3, 4, inputDirection);

            // Act
            var result = position.Left();

            // Assert
            Assert.That(result.XCoordinate, Is.EqualTo(position.XCoordinate));
            Assert.That(result.YCoordinate, Is.EqualTo(position.YCoordinate));
            Assert.That(result.FacingDirection, Is.EqualTo(expectedResult));
        }

        [TestCase(CardinalDirection.North)]
        [TestCase(CardinalDirection.South)]
        [TestCase(CardinalDirection.East)]
        [TestCase(CardinalDirection.West)]
        public void RightFollowedByLeft_StaysSame(CardinalDirection inputDirection)
        {
            // Arrange
            var position = new Position(3, 4, inputDirection);

            // Act
            var result = position.Right().Left();

            // Assert
            Assert.That(result.XCoordinate, Is.EqualTo(position.XCoordinate));
            Assert.That(result.YCoordinate, Is.EqualTo(position.YCoordinate));
            Assert.That(result.FacingDirection, Is.EqualTo(position.FacingDirection));
        }
    }
}