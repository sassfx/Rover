using NUnit.Framework;
using Rover.Core;

namespace Core.Tests
{
    [TestFixture]
    public class GridTest
    {
        [TestCase(1, 1, 1, 1)]
        [TestCase(10, 10, 10, 10)]
        [TestCase(11, 10, 0, 10)]
        [TestCase(8, 11, 8, 0)]
        [TestCase(-2, 10, 9, 10)]
        [TestCase(0, -1, 0, 10)]
        public void WrapPosition(int xPosition, int yPosition, int expectedXPosition, int expectedYPosition)
        {
            // Arrange
            var grid = new Grid(10, 10);
            var postion = new Position(xPosition, yPosition, CardinalDirection.East);

            // Act
            var wrappedPosition = grid.WrapPosition(postion);

            // Assert
            Assert.That(wrappedPosition.XCoordinate, Is.EqualTo(expectedXPosition));
            Assert.That(wrappedPosition.YCoordinate, Is.EqualTo(expectedYPosition));
            Assert.That(wrappedPosition.FacingDirection, Is.EqualTo(postion.FacingDirection));
        }

        [Test]
        public void IsPositionFree_NoObstacle()
        {
            // Arrange
            var grid = new Grid(10, 10);
            var postion = new Position(1, 2, CardinalDirection.East);

            // Act
            IObstacle obstacle;
            var result = grid.IsPositionFree(postion, out obstacle);

            // Assert
            Assert.That(result, Is.True);
            Assert.That(obstacle, Is.Null);
        }

        [Test]
        public void IsPositionFree_Obstacle()
        {
            // Arrange
            var grid = new Grid(10, 10);
            var xCoordinate = 1;
            var yCoordindate = 2;
            var postion = new Position(xCoordinate, yCoordindate, CardinalDirection.East);
            var obstacle = new Obstacle("Barrier");
            grid.AddObstacle(obstacle, xCoordinate, yCoordindate);

            // Act
            IObstacle obstacleAtPosition;
            var result = grid.IsPositionFree(postion, out obstacleAtPosition);

            // Assert
            Assert.That(result, Is.False);
            Assert.That(obstacle, Is.SameAs(obstacle));
        }
    }
}