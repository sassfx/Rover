using NUnit.Framework;
using Rover.Core;

namespace Core.Tests
{
    [TestFixture]
    public class RoverIntegrationTests
    {
        [Test]
        public void FullIntegrationTest()
        {
            // Arrange
            var position = new Position(0, 0, CardinalDirection.North);
            var rover = new Rover.Core.Rover(position);
            var grid = new Grid(100, 100);
            var commandExecutor = new RoverMoveCommandsExecutor(rover, grid);

            // Act
            var result = commandExecutor.ExecuteRoverCommands("FFRFF");

            // Assert
            Assert.That(result.Successful, Is.True);
            Assert.That(result.EncounteredObstacle, Is.Null);
            Assert.That(rover.CurrentPosition.XCoordinate, Is.EqualTo(2));
            Assert.That(rover.CurrentPosition.YCoordinate, Is.EqualTo(2));
            Assert.That(rover.CurrentPosition.FacingDirection, Is.EqualTo(CardinalDirection.East));
        }

        [Test]
        public void FullIntegrationTest_WithWrap()
        {
            // Arrange
            var position = new Position(3, 3, CardinalDirection.North);
            var rover = new Rover.Core.Rover(position);
            var grid = new Grid(5, 5);
            var commandExecutor = new RoverMoveCommandsExecutor(rover, grid);

            // Act
            var result = commandExecutor.ExecuteRoverCommands("FFFRFFFL");

            // Assert
            Assert.That(result.Successful, Is.True);
            Assert.That(result.EncounteredObstacle, Is.Null);
            Assert.That(rover.CurrentPosition.XCoordinate, Is.EqualTo(0));
            Assert.That(rover.CurrentPosition.YCoordinate, Is.EqualTo(0));
            Assert.That(rover.CurrentPosition.FacingDirection, Is.EqualTo(CardinalDirection.North));
        }

        [Test]
        public void FullIntegrationTest_WithObstacles()
        {
            // Arrange
            var position = new Position(0, 0, CardinalDirection.North);
            var rover = new Rover.Core.Rover(position);
            var grid = new Grid(100, 100);
            var obstacle = new Obstacle("Barrier");
            grid.AddObstacle(obstacle, 2, 2);
            var commandExecutor = new RoverMoveCommandsExecutor(rover, grid);

            // Act
            var result = commandExecutor.ExecuteRoverCommands("FFRFFF");

            // Assert
            Assert.That(result.Successful, Is.False);
            Assert.That(result.EncounteredObstacle, Is.EqualTo(obstacle));
            Assert.That(rover.CurrentPosition.XCoordinate, Is.EqualTo(1));
            Assert.That(rover.CurrentPosition.YCoordinate, Is.EqualTo(2));
            Assert.That(rover.CurrentPosition.FacingDirection, Is.EqualTo(CardinalDirection.East));
        }
    }
}