﻿using System;
using NUnit.Framework;
using Rhino.Mocks;
using Rover.Core;

namespace Core.Tests
{
    [TestFixture]
    public class RoverMoveCommandsExecutorTests
    {
        [Test]
        public void ExecuteCommands_F_CallsForwards()
        {
            // Arrange
            IPosition position = MockRepository.GenerateMock<IPosition>();
            IPosition finalPosition = MockRepository.GenerateStub<IPosition>();
            position.Stub(x => x.Forwards()).Return(finalPosition);
            IRover rover = MockRepository.GenerateMock<IRover>();
            rover.Stub(x => x.CurrentPosition).Return(position);
            IGrid grid = MockRepository.GenerateMock<IGrid>();
            grid.Stub(x => x.WrapPosition(Arg<IPosition>.Is.Anything))
                .Return(null) // Rhino requires this
                .WhenCalled(_ => _.ReturnValue = _.Arguments[0]);
            grid.Stub(x => x.IsPositionFree(Arg<IPosition>.Is.Anything, out Arg<IObstacle>.Out(null).Dummy)).Return(true);
            var roverCommandExecutor = new RoverMoveCommandsExecutor(rover, grid);

            // Act
            roverCommandExecutor.ExecuteRoverCommands("F");

            // Assert
            position.AssertWasCalled(x => x.Forwards());
            rover.AssertWasCalled(x => x.UpdatePosition(finalPosition));
        }

        [Test]
        public void ExecuteCommands_B_CallsBackwards()
        {
            // Arrange
            IPosition position = MockRepository.GenerateMock<IPosition>();
            IPosition finalPosition = MockRepository.GenerateStub<IPosition>();
            position.Stub(x => x.Backwards()).Return(finalPosition);
            IRover rover = MockRepository.GenerateMock<IRover>();
            rover.Stub(x => x.CurrentPosition).Return(position);
            IGrid grid = MockRepository.GenerateMock<IGrid>();
            grid.Stub(x => x.WrapPosition(Arg<IPosition>.Is.Anything))
                .Return(null) // Rhino requires this
                .WhenCalled(_ => _.ReturnValue = _.Arguments[0]);
            grid.Stub(x => x.IsPositionFree(Arg<IPosition>.Is.Anything, out Arg<IObstacle>.Out(null).Dummy)).Return(true);
            var roverCommandExecutor = new RoverMoveCommandsExecutor(rover, grid);

            // Act
            roverCommandExecutor.ExecuteRoverCommands("B");

            // Assert
            position.AssertWasCalled(x => x.Backwards());
            rover.AssertWasCalled(x => x.UpdatePosition(finalPosition));
        }

        [Test]
        public void ExecuteCommands_R_CallsRight()
        {
            // Arrange
            IPosition position = MockRepository.GenerateMock<IPosition>();
            IPosition finalPosition = MockRepository.GenerateStub<IPosition>();
            position.Stub(x => x.Right()).Return(finalPosition);
            IRover rover = MockRepository.GenerateMock<IRover>();
            rover.Stub(x => x.CurrentPosition).Return(position);
            IGrid grid = MockRepository.GenerateMock<IGrid>();
            grid.Stub(x => x.WrapPosition(Arg<IPosition>.Is.Anything))
                .Return(null) // Rhino requires this
                .WhenCalled(_ => _.ReturnValue = _.Arguments[0]);
            grid.Stub(x => x.IsPositionFree(Arg<IPosition>.Is.Anything, out Arg<IObstacle>.Out(null).Dummy)).Return(true);
            var roverCommandExecutor = new RoverMoveCommandsExecutor(rover, grid);

            // Act
            var result = roverCommandExecutor.ExecuteRoverCommands("R");

            // Assert
            Assert.That(result.Successful, Is.True);
            Assert.That(result.EncounteredObstacle, Is.Null);
            Assert.That(result.NewPosition, Is.EqualTo(finalPosition));
            position.AssertWasCalled(x => x.Right());
            rover.AssertWasCalled(x => x.UpdatePosition(finalPosition));
        }

        [Test]
        public void ExecuteCommands_L_CallsLeft()
        {
            // Arrange
            IPosition position = MockRepository.GenerateMock<IPosition>();
            IPosition finalPosition = MockRepository.GenerateStub<IPosition>();
            position.Stub(x => x.Left()).Return(finalPosition);
            IRover rover = MockRepository.GenerateMock<IRover>();
            rover.Stub(x => x.CurrentPosition).Return(position);
            IGrid grid = MockRepository.GenerateMock<IGrid>();
            grid.Stub(x => x.WrapPosition(Arg<IPosition>.Is.Anything))
                .Return(null) // Rhino requires this
                .WhenCalled(_ => _.ReturnValue = _.Arguments[0]);
            grid.Stub(x => x.IsPositionFree(Arg<IPosition>.Is.Anything, out Arg<IObstacle>.Out(null).Dummy)).Return(true);
            var roverCommandExecutor = new RoverMoveCommandsExecutor(rover, grid);

            // Act
            var result = roverCommandExecutor.ExecuteRoverCommands("L");

            // Assert
            Assert.That(result.Successful, Is.True);
            Assert.That(result.EncounteredObstacle, Is.Null);
            Assert.That(result.NewPosition, Is.EqualTo(finalPosition));
            position.AssertWasCalled(x => x.Left());
            rover.AssertWasCalled(x => x.UpdatePosition(finalPosition));
        }

        [Test]
        public void ExecuteCommands_InvalidCommand_ThrowsException()
        {
            // Arrange
            IRover rover = MockRepository.GenerateMock<IRover>();
            IGrid grid = MockRepository.GenerateMock<IGrid>();
            var roverCommandExecutor = new RoverMoveCommandsExecutor(rover, grid);

            // Act & Assert
            Assert.Throws<ArgumentOutOfRangeException>(() => roverCommandExecutor.ExecuteRoverCommands("T"));
        }

        [Test]
        public void ExecuteCommands_CompositeCommands()
        {
            // Arrange
            IPosition position = MockRepository.GenerateMock<IPosition>();
            IPosition secondPosition = MockRepository.GenerateStub<IPosition>();
            IPosition thirdPosition = MockRepository.GenerateStub<IPosition>();
            IPosition fourthPosition = MockRepository.GenerateStub<IPosition>();
            IPosition finalPosition = MockRepository.GenerateStub<IPosition>();
            position.Stub(x => x.Forwards()).Return(secondPosition);
            secondPosition.Stub(x => x.Forwards()).Return(thirdPosition);
            thirdPosition.Stub(x => x.Right()).Return(fourthPosition);
            fourthPosition.Stub(x => x.Backwards()).Return(finalPosition);
            IRover rover = MockRepository.GenerateMock<IRover>();
            rover.Stub(x => x.CurrentPosition).Return(position);
            IGrid grid = MockRepository.GenerateMock<IGrid>();
            grid.Stub(x => x.WrapPosition(Arg<IPosition>.Is.Anything))
                .Return(null) // Rhino requires this
                .WhenCalled(_ => _.ReturnValue = _.Arguments[0]);
            grid.Stub(x => x.IsPositionFree(Arg<IPosition>.Is.Anything, out Arg<IObstacle>.Out(null).Dummy)).Return(true);
            var roverCommandExecutor = new RoverMoveCommandsExecutor(rover, grid);

            // Act
            var result = roverCommandExecutor.ExecuteRoverCommands("FFRB");

            // Assert
            Assert.That(result.Successful, Is.True);
            Assert.That(result.EncounteredObstacle, Is.Null);
            Assert.That(result.NewPosition, Is.EqualTo(finalPosition));
            position.AssertWasCalled(x => x.Forwards());
            secondPosition.AssertWasCalled(x => x.Forwards());
            thirdPosition.AssertWasCalled(x => x.Right());
            fourthPosition.AssertWasCalled(x => x.Backwards());
            rover.AssertWasCalled(x => x.UpdatePosition(finalPosition));
        }
    }
}